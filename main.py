from selenium import webdriver
from time import sleep
from secrets import pw
import random


class InstaBot:

    minute=60
    default=45
    def __init__(self, username, pw):
        self.driver = webdriver.Chrome()
        self.username = username
        self.driver.get("https://instagram.com")
        sleep(2)
        self.driver.find_element_by_xpath("//a[contains(text(), 'Log in')]")\
            .click()
        sleep(2)
        self.driver.find_element_by_xpath("//input[@name=\"username\"]")\
            .send_keys(username)
        self.driver.find_element_by_xpath("//input[@name=\"password\"]")\
            .send_keys(pw)
        self.driver.find_element_by_xpath('//button[@type="submit"]')\
            .click()
        sleep(4)
        self.driver.find_element_by_xpath("//button[contains(text(), 'Not Now')]")\
            .click()
        sleep(2)

    def goToInsta(self):
        # self.driver.get("https://www.instagram.com/p/B73rwiQpoYA/")
        #self.driver.get("https://www.instagram.com/p/B8cOCMjB2n7/")
        self.driver.get("https://www.instagram.com/p/B9FhkbThyUZ/")
        sleep(1)
        self.AutoWin()


    def SendMessenge(self):
        self.driver.find_element_by_xpath("/html/body/div[1]/section/main/div/div/article/div[2]/section[3]/div/form/textarea")\
            .click()
        self.driver.find_element_by_xpath("/html/body/div[1]/section/main/div/div/article/div[2]/section[3]/div/form/textarea")\
            .send_keys(self.get_names())
        self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div/article/div[2]/section[3]/div[1]/form/button')\
            .click()

    def get_names(self):
        first = ( "@luciamatni ","@rochaa_felipe ","@fabio.rocha.araujo ","@heyheylucy ")
        group = random.sample(first,k=2)
        return group

    # def click_retry(self):
    #     # popup_3 = self.driver.find_element_by_xpath('//button[contains("Retry")]')
    #     popup_3 = self.driver.find_elements_by_xpath("/html/body/div[2]/div/div/button")
    #     popup_3.click()

    # def wait_to_send(self):
    #     sleep(self.minute*15)
    #     self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div/article/div[2]/section[3]/div[1]/form/button')\
    #         .click()

    # def wait_more_to_send(self):
    #     sleep(self.minute*30)
    #     self.driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/div/article/div[2]/section[3]/div[1]/form/button')\
    #         .click()
    def AutoWin(self):
        for i in range(500):
            sleep(1)
            try:
                sleep(self.minute)
                self.SendMessenge()
                print(f"1 min: {i}")
            except Exception:
                try:
                    print("Exception Wait 15 min please ...")
                    sleep(self.minute*15)
                    self.SendMessenge()
                    # self.wait_to_send()
                    print(f"15 min: {i}")
                except Exception:
                    print("Exception Wait 45 min please ...")
                    sleep(self.minute*45)
                    self.SendMessenge()                    
                    # self.wait_more_to_send()
                    print(f"30 min: {i}")

my_bot = InstaBot('nmatni', pw)
my_bot.get_names()
my_bot.goToInsta()
